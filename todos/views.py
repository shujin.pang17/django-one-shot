from django.shortcuts import redirect, render, get_object_or_404, reverse
from todos.models import TodoList, TodoItem
from django.contrib.auth.decorators import login_required
from todos.forms import TodoListForm, TodoItemForm


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(
            request.POST, instance=todo
        )  # here we are making sure we replace existing data
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(
            instance=todo
        )  # here we are making sure we show existing data
        context = {"form": form, "todo_object": todo}

    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

# model TodoItem functions:

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }

    return render(request, "items/create.html", context)


def todo_item_edit(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(
            request.POST, instance=item
        )  # here we are making sure we replace existing data
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(
            instance=item
        )  # here we are making sure we show existing data
        context = {"form": form, "item_object": item}

    return render(request, "items/edit.html", context)
